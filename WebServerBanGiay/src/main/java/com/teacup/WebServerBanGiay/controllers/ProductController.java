package com.teacup.WebServerBanGiay.controllers;

import com.teacup.WebServerBanGiay.Lib.ResponseData;
import com.teacup.WebServerBanGiay.models.*;
import com.teacup.WebServerBanGiay.models.datas.FormAddSize;
import com.teacup.WebServerBanGiay.models.datas.FormProduct;
import com.teacup.WebServerBanGiay.models.formView.UpdateProduct;
import com.teacup.WebServerBanGiay.repositories.CategoryRepository;
import com.teacup.WebServerBanGiay.repositories.ProductRepository;
import com.teacup.WebServerBanGiay.repositories.ProviderRepository;
import com.teacup.WebServerBanGiay.services.IStorageService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    public ProductRepository productRepository;
    @Autowired
    public IStorageService iStorageService;
    @Autowired
    public CategoryRepository categoryRepository;
    @Autowired
    public ProviderRepository providerRepository;

//    public String imageNameProduct;

    @GetMapping("/getAllProduct")
    public ResponseEntity<ResponseObject> getAllProduct() {
        try {
            List<Product> productList = productRepository.findAll();
            return ResponseData.returnSuccessData("Danh sách sản phẩm", productList);

        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    @GetMapping("/getProductByCategoryID")
    public ResponseEntity<ResponseObject> getProductBuCategoryID(@RequestParam String categoryId)  {
        try {
            if(categoryId.trim().isEmpty()) {
                return ResponseData.returnErrorData("Vui lòng điền Id thể loại");
            }

            List<Product> productList = productRepository.findProductByIdCategory(categoryId);
            return ResponseData.returnSuccessData("Danh sách sản phẩm", productList);

        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    @GetMapping("/getProductByProviderID")
    public ResponseEntity<ResponseObject> getProductByProviderID(@RequestParam String providerId) {
        try {
            if(providerId.trim().isEmpty()) {
                return ResponseData.returnErrorData("Vui lòng điện Id nhà cung cấp");
            }
            List<Product> productList = productRepository.findProductByIdProvider(providerId);
            return ResponseData.returnSuccessData("", productList);

        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    @GetMapping("/image/{fileName:.+}")
    public ResponseEntity<byte[]> readImageProduct(@PathVariable String fileName) {
        try {
            byte[] bytes = iStorageService.readFileContent(fileName);
            return  ResponseEntity.status(HttpStatus.OK).contentType(MediaType.IMAGE_JPEG).body(bytes);
        } catch (Exception e) {
            return  ResponseEntity.noContent().build();
        }
    }

    @PostMapping(path = "/addProduct", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ResponseObject> addProduct(@ModelAttribute FormProduct formProduct ) {
        try {
            if (checkFieldAddProduct(formProduct)) {
                return ResponseData.returnErrorData("Vui lòng điền đầy đủ thông tin sản phẩm");
            }

            Optional<Category> category = categoryRepository.findById(formProduct.getCategoryId());
            if (category.isEmpty()) {
                return ResponseData.returnErrorData("Thể loại không tồn tại");
            }

            Optional<Provider> provider = providerRepository.findById(formProduct.getProviderId());
            if (provider.isEmpty()) {
                return ResponseData.returnErrorData("Nhà cung cấp không tồn tại");
            }

            String imageNameProduct = iStorageService.storeFile(formProduct.getFile());

            Product product = productRepository.save(
                    createProduct(formProduct, category.get(), provider.get(), imageNameProduct)
            );

            return ResponseData.returnSuccessData("Đã thêm sản phẩm thành công",product);

        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    @PutMapping("/addSizeProduct")
    public ResponseEntity<ResponseObject> addSizeProduct(@RequestBody FormAddSize formAddSize) {
        try {
            boolean productId = formAddSize.get_id().trim().isEmpty();
            if (productId) {
                ResponseData.returnErrorData("Vui lòng điền đầy đủ thông tin sản phẩm");
            }

            Optional<Product> productOptional = productRepository.findById(formAddSize.get_id());
            if (productOptional.isEmpty()) {
                return ResponseData.returnErrorData("Sản phẩm không tồn tại");
            }

            Product product = productRepository.save(
                    createSizeProduct(formAddSize, productOptional.get())
            );

            return ResponseData.returnSuccessData("thêm size giày thành công", product);

        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    @PutMapping("/updateProduct")
    public ResponseEntity<ResponseObject> updateProduct(@RequestBody com.teacup.WebServerBanGiay.models.formView.UpdateProduct updateProduct) {
        try {
            if (checkFieldUpdateProduct(updateProduct)) {
                return ResponseData.returnErrorData("Vui lòng điền đầy đủ thông tin");
            }

            Optional<Category> categoryOptional = categoryRepository.findById(updateProduct.getCategoryId());
            if(categoryOptional.isEmpty()) {
                return ResponseData.returnErrorData("ID thể loại không tồn tại");
            }

            Optional<Provider> providerOptional = providerRepository.findById(updateProduct.getProviderId());
            if(providerOptional.isEmpty()) {
                return  ResponseData.returnErrorData(("ID nhà cung cấp không tồn tại"));
            }

            Optional<Product> productOptional = productRepository.findById(updateProduct.get_id());
            if(productOptional.isEmpty()) {
                return ResponseData.returnErrorData(("Id sản phẩm không tồn tại"));
            }

            Product product = productRepository.save(
                    updateProduct(updateProduct, productOptional.get(), categoryOptional.get(), providerOptional.get())
            );
            return ResponseData.returnSuccessData("Sửa thông tin thành công",product);
        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    private boolean checkFieldAddProduct(FormProduct formProduct) {
        boolean productname = formProduct.getProductName().trim().isEmpty();
        boolean categoryId = formProduct.getCategoryId().trim().isEmpty();
        boolean providerId = formProduct.getProviderId().trim().isEmpty();
        return (productname || categoryId || providerId);
    }

   private Product createProduct(
           FormProduct formProduct, Category category, Provider provider, String imageNameProduct) {

       Product product = new Product();
       product.setProductName(formProduct.getProductName());
       product.setPrice(formProduct.getPrice());
       product.setImageProduct("http://localhost:8080/api/product/image/"+ imageNameProduct);
       product.setCategory(category);
       product.setProvider(provider);
       product.setStatus(false);

        return product;
   }

   private Product createSizeProduct(FormAddSize formAddSize, Product product) {

       Details details = new Details();
       details.setSizeId(new ObjectId().toString());
       details.setSize(formAddSize.getSize());
       details.setQuantityInStock(formAddSize.getQuantityInStock());
       List<Details> detailsList = product.getDetails();
       if (detailsList == null) {
           detailsList = new ArrayList<>();
       }
       detailsList.add(details);
       product.setDetails(detailsList);

       return product;
   }

   private boolean checkFieldUpdateProduct(UpdateProduct updateProduct) {
        boolean productId = updateProduct.get_id().trim().isEmpty();
        boolean productName = updateProduct.getProductName().trim().isEmpty();
        boolean categoryId = updateProduct.getCategoryId().trim().isEmpty();
        boolean providerId = updateProduct.getProviderId().trim().isEmpty();

        return (productId || productName || categoryId || providerId);
   }

   private Product updateProduct(UpdateProduct updateProduct, Product product, Category category, Provider provider) {
        product.setProductName(updateProduct.getProductName());
        product.setPrice(updateProduct.getPrice());
        product.setCategory(category);
        product.setProvider(provider);
        product.setDetails(updateProduct.getDetails());

        return product;
   }
}
