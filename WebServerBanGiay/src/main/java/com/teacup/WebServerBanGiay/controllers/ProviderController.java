package com.teacup.WebServerBanGiay.controllers;

import com.teacup.WebServerBanGiay.Lib.ResponseData;
import com.teacup.WebServerBanGiay.models.Provider;
import com.teacup.WebServerBanGiay.models.ResponseObject;
import com.teacup.WebServerBanGiay.repositories.ProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/provider")

public class ProviderController {

    @Autowired
    public ProviderRepository providerRepository;

    @GetMapping("/getAllProvider")
    public ResponseEntity<ResponseObject> getAllProvider() {
        try {
            List<Provider> providerList = providerRepository.findAll();
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(false, "Danh sách nhà cung cấp", providerList)
            );
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(true, e.getMessage(), "")
            );
        }
    }

    @PostMapping("/addProvider")
    public ResponseEntity<ResponseObject> addProvider(@RequestBody Provider newProvider) {
        try {
            if (checkFieldAddProvider(newProvider)) {
                return ResponseData.returnErrorData("Vui lòng điền đầy đủ thông tin");
            }

            Provider provider = providerRepository.save(newProvider);
            return ResponseData.returnSuccessData("Đã thêm thể loại thành công", provider);

        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    @PutMapping("/updateProvider")
    public ResponseEntity<ResponseObject> updateProviderById(@RequestBody Provider updateProvider) {
        try {
            if (checkFieldUpdateProvider(updateProvider)) {
                return ResponseData.returnErrorData("Vui lòng điền đầy đủ thông tin");
            }

            Optional<Provider> providerOptional = providerRepository.findById(updateProvider.get_id());
            if(providerOptional.isEmpty()) {
                return ResponseData.returnErrorData("Id nhà cung cấp không đúng");
            }

            Provider provider = providerRepository.save(updateProvider);
            return ResponseData.returnSuccessData("Sửa nhà cung cấp thành công", provider);

        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    private boolean checkFieldAddProvider(Provider provider) {
        boolean providerName = provider.getProviderName().trim().isEmpty();
        boolean address = provider.getAddress().trim().isEmpty();
        boolean phone = provider.getPhone().trim().isEmpty();
        return ( providerName || address || phone);
    }

    private boolean checkFieldUpdateProvider(Provider provider) {
        boolean provideId = provider.get_id().trim().isEmpty();
        boolean providerName = provider.getProviderName().trim().isEmpty();
        boolean address = provider.getAddress().trim().isEmpty();
        boolean phone = provider.getPhone().trim().isEmpty();
        return (provideId || providerName || address || phone);
    }
}
