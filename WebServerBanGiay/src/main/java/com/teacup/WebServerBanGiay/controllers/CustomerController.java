package com.teacup.WebServerBanGiay.controllers;

import com.teacup.WebServerBanGiay.Lib.ResponseData;
import com.teacup.WebServerBanGiay.Lib.Util;
import com.teacup.WebServerBanGiay.models.Customer;
import com.teacup.WebServerBanGiay.models.datas.ChangePasswordCustomer;
import com.teacup.WebServerBanGiay.models.ResponseObject;
import com.teacup.WebServerBanGiay.models.datas.ResetPasswordData;
import com.teacup.WebServerBanGiay.repositories.CustomerRepository;
import com.teacup.WebServerBanGiay.services.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    @Autowired
    public CustomerRepository customerRepository;

    @Autowired
    public Mail mail;

    @PostMapping("/login")
    public ResponseEntity<ResponseObject> login(@RequestBody Customer customer) {
        try {
            String email = customer.getEmail().trim();
            String password = customer.getPassword().trim();

            ArrayList<String> arr = new ArrayList<>();
            arr.add(email);
            arr.add(password);
            // return error if email or password is empty
            if(Util.isEmptyFields(arr)) return ResponseData.returnErrorData("email hoặc mật khẩu không được để trống");

            // find user
            Optional<Customer> data = customerRepository.findByEmail(email);

            // empty user
            if(data.isEmpty()) return ResponseData.returnErrorData("email không chính xác"); // empty user

            Customer user = data.get();
            if(!user.getPassword().equals(password)) return ResponseData.returnErrorData("email không chính xác");
            else return ResponseData.returnSuccessData("đăng nhập thành công",user);

        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    @PostMapping("/signup")
    public ResponseEntity<ResponseObject> register(@RequestBody Customer customer) {
        try {
            String email = customer.getEmail().trim();
            String password = customer.getPassword().trim();

            ArrayList<String> arr = new ArrayList<>();
            arr.add(email);
            arr.add(password);
            // return error if email or password is empty
            if(Util.isEmptyFields(arr)) return ResponseData.returnErrorData("email hoặc mật khẩu không được để trống");

            // find user
            Optional<Customer> data = customerRepository.findByEmail(email);

            // email has registered before
            if(data.isPresent()) return ResponseData.returnErrorData("email đã được đăng ký");

            Customer user = customerRepository.save(customer);
            return ResponseData.returnSuccessData("đăng ký thành công", user);
        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    @PutMapping("/edit-customer")
    public ResponseEntity<ResponseObject> editCustomer(@RequestBody Customer customer) {
        try {
            String email = customer.getEmail().trim();
            String _id = customer.get_id().trim();

            ArrayList<String> arr = new ArrayList<>();
            arr.add(email);
            arr.add(_id);
            if(Util.isEmptyFields(arr)) return ResponseData.returnErrorData("email hoặc id không được để trống");

            // find user
            Optional<Customer> data = customerRepository.findById(_id);

            // email has registered before
            if(data.isEmpty()) return ResponseData.returnErrorData("không tìm thấy người dùng");

            Customer user = customerRepository.save(customer);
            return ResponseData.returnSuccessData("chỉnh sửa thông tin thành công", user);
        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    @PutMapping("/change-password")
    public ResponseEntity<ResponseObject> changePassword(@RequestBody ChangePasswordCustomer customer) {
        try {
            String customerId = customer.getCustomer_id().trim();
            String oldPassword = customer.getOld_password().trim();
            String newPassword = customer.getNew_password().trim();

            ArrayList<String> arr = new ArrayList<>();
            arr.add(customerId);
            arr.add(oldPassword);
            arr.add(newPassword);
            if(Util.isEmptyFields(arr))
                return ResponseData.returnErrorData("Vui lòng điền đủ 3 trường id, mật khẩu cũ, mật khẩu mới");

            // find user
            Optional<Customer> data = customerRepository.findById(customerId);

            // no user found
            if(data.isEmpty()) return ResponseData.returnErrorData("không tìm thấy người dùng");

            // get customer
            Customer user = data.get();

            // password is not correct
            if(!user.getPassword().equals(oldPassword))
                return ResponseData.returnErrorData("mật khẩu cũ không khớp");

            //update password's user
            user.setPassword(newPassword);
            customerRepository.save(user);
            return ResponseData.returnSuccessData("chỉnh sửa thông tin thành công", user);
        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    @PutMapping("/forgot-password/receiveOTP")
    public ResponseEntity<ResponseObject> receiveOTP(@RequestBody Customer customer) {
        try {
            String email = customer.getEmail().trim();
            if(email.isEmpty()) return ResponseData.returnErrorData("email không được để trống");

            // find user
            Optional<Customer> data = customerRepository.findByEmail(email);

            // empty user
            if(data.isEmpty()) return ResponseData.returnErrorData("email không tồn tại"); // empty user

            // generate OTP
            Random rand = new Random();
            int otp = rand.nextInt(89999) + 10000; // OTR from 10000 to 99999

            // save OTP
            Customer user = data.get();
            user.setResetCode(otp);
            customerRepository.save(user);

            //send mail
            if(mail.sendMail("Mã OTP của bạn là: " + otp,email))
                return ResponseData.returnSuccessData("Gửi mail thành công","");
            else return ResponseData.returnErrorData("Gửi mail thất bại");
        }catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    @PutMapping("/forgot-password/resetPassword")
    public ResponseEntity<ResponseObject> resetPassword(@RequestBody ResetPasswordData resetPasswordData) {
        try {
            String email = resetPasswordData.getEmail().trim();
            String resetCode = resetPasswordData.getResetCode().trim();
            String password = resetPasswordData.getNewPassword().trim();

            ArrayList<String> arr = new ArrayList<>();
            arr.add(email);
            arr.add(password);
            arr.add(resetCode);

            // return error if email or password is empty
            if(Util.isEmptyFields(arr))
                return ResponseData.returnErrorData("email, resetCode hoặc mật khẩu không được để trống");

            // find user
            Optional<Customer> data = customerRepository.findByEmail(email);

            // empty user
            if(data.isEmpty()) return ResponseData.returnErrorData("email không tồn tại"); // empty user

            Customer user = data.get();
            if(user.getResetCode() != 0 && user.getResetCode() == Integer.parseInt(resetCode)){
                user.setPassword(password);
                user.setResetCode(0);
                customerRepository.save(user);
                return ResponseData.returnSuccessData("Thay đổi mật khẩu thành công","");
            }
            return ResponseData.returnErrorData("Thay đổi mật khẩu thất bại, mã OTP không khớp");
        }catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

}
