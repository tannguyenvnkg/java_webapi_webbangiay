package com.teacup.WebServerBanGiay.controllers;

import com.teacup.WebServerBanGiay.Lib.ResponseData;
import com.teacup.WebServerBanGiay.models.Category;
import com.teacup.WebServerBanGiay.models.ResponseObject;
import com.teacup.WebServerBanGiay.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping ("/api/category")
public class CategoryController {

    @Autowired
    public  CategoryRepository categoryRepository;

    @GetMapping("/getAllCategory")
    public ResponseEntity<ResponseObject> getAllCategory() {
        try {
            List<Category> categoryList = categoryRepository.findAll();
            return ResponseData.returnSuccessData("Danh sách tất cả thể loại", categoryList);

        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    @PostMapping("/addCategory")
    public ResponseEntity<ResponseObject> addCategory(@RequestBody Category newCategory) {
        try {
            boolean categoryName = newCategory.getCategoryName().trim().isEmpty();
            if (categoryName) {
                return ResponseData.returnErrorData("Vui lòng điền tên thể loại");
            }

            Category category = categoryRepository.save(newCategory);
            return ResponseData.returnSuccessData("Đã thêm thể loại thành công", newCategory);

        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    @PutMapping("/updateCategory")
    public ResponseEntity<ResponseObject> updateCategoryById(@RequestBody Category updateCategory) {
        try {
            if(checkCategoryEmpty(updateCategory)) {
                return ResponseData.returnErrorData("Vui lòng điền đầy đủ thông tin");
            }

            Optional<Category> categoryOptional = categoryRepository.findById(updateCategory.get_id().trim());

            if (categoryOptional.isEmpty()) {
                return ResponseData.returnErrorData("Id thể loại không đúng");
            }

            Category category =  categoryRepository.save(updateCategory);
            return ResponseData.returnSuccessData("Sửa thể loại thành công", category);

        } catch (Exception e) {
            return ResponseData.returnErrorData(e.getMessage());
        }
    }

    private boolean checkCategoryEmpty(Category category) {
        boolean categoryId = category.get_id().trim().isEmpty();
        boolean categoryName = category.getCategoryName().trim().isEmpty();
        return (categoryId || categoryName);
    }

}
