package com.teacup.WebServerBanGiay.repositories;


import com.teacup.WebServerBanGiay.models.Provider;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProviderRepository extends MongoRepository<Provider, String> {

}
