package com.teacup.WebServerBanGiay.repositories;

import com.teacup.WebServerBanGiay.models.Category;
import com.teacup.WebServerBanGiay.models.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends MongoRepository<Product, String> {

    @Query("{'category._id': ?0}")
    List<Product> findProductByIdCategory (String categoryId);
    @Query("{'provider._id': ?0}")
    List<Product> findProductByIdProvider (String providerId);
}
