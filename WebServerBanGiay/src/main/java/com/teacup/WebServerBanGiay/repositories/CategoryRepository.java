package com.teacup.WebServerBanGiay.repositories;

import com.teacup.WebServerBanGiay.models.Category;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends MongoRepository<Category, String> {

}
