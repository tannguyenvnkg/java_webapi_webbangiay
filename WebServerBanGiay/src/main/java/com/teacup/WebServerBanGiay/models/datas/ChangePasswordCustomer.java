package com.teacup.WebServerBanGiay.models.datas;

public class ChangePasswordCustomer {
    private String customer_id;
    private String new_password;
    private String old_password;

    public ChangePasswordCustomer(String customer_id, String new_password, String old_password) {
        this.customer_id = customer_id;
        this.new_password = new_password;
        this.old_password = old_password;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getOld_password() {
        return old_password;
    }

    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }

    @Override
    public String toString() {
        return "ChangePasswordCustomer{" +
                "customer_id='" + customer_id + '\'' +
                ", new_password='" + new_password + '\'' +
                ", old_password='" + old_password + '\'' +
                '}';
    }
}
