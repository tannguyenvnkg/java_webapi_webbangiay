package com.teacup.WebServerBanGiay.models;


import org.springframework.data.annotation.Id;


public class Details {
    // Đây là details gồm số size giày và số lượng tồn kho

    @Id
    private String sizeId;
    private int size;
    private int quantityInStock;

    public Details() {

    }

    public Details(String sizeId, int size, int quantityInStock) {
        this.sizeId = sizeId;
        this.size = size;
        this.quantityInStock = quantityInStock;
    }

    public String getSizeId() {
        return sizeId;
    }

    public void setSizeId(String sizeId) {
        this.sizeId = sizeId;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    @Override
    public String toString() {
        return "Details{" +
                "sizeId='" + sizeId + '\'' +
                ", size=" + size +
                ", quantityInStock=" + quantityInStock +
                '}';
    }
}
