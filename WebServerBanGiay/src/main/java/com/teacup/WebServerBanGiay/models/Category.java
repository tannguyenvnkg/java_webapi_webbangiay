package com.teacup.WebServerBanGiay.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "categories")
public class Category {

    @Id
    private String _id;
    private String categoryName;

    public Category() {

    }

    public Category(String _id, String categoryName) {
        this._id = _id;
        this.categoryName = categoryName;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "Category{" +
                "_id='" + _id + '\'' +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}
