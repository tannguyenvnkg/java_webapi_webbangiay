package com.teacup.WebServerBanGiay.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "products")
public class Product {

    @Id
    private String _id;
    private String productName;
    private String imageProduct;
    private int price;
    private Category category;
    private Provider provider;
    private boolean status;
    private Detail detail;
    private List<Details> details;

    public Product() {

    }

    public Product(String _id, String productName, String imageProduct, int price, Category category, Provider provider, boolean status, Detail detail, List<Details> details) {
        this._id = _id;
        this.productName = productName;
        this.imageProduct = imageProduct;
        this.price = price;
        this.category = category;
        this.provider = provider;
        this.status = status;
        this.detail = detail;
        this.details = details;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getImageProduct() {
        return imageProduct;
    }

    public void setImageProduct(String imageProduct) {
        this.imageProduct = imageProduct;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public List<Details> getDetails() {
        return details;
    }

    public void setDetails(List<Details> details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "Product{" +
                "_id='" + _id + '\'' +
                ", productName='" + productName + '\'' +
                ", imageProduct='" + imageProduct + '\'' +
                ", price=" + price +
                ", category=" + category +
                ", provider=" + provider +
                ", status=" + status +
                ", detail=" + detail +
                ", details=" + details +
                '}';
    }
}
