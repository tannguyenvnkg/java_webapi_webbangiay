package com.teacup.WebServerBanGiay.models.datas;

public class FormAddSize {
    private String _id;
    private int size;
    private int quantityInStock;

    public FormAddSize() {

    }

    public FormAddSize(String _id, int size, int quantityInStock) {
        this._id = _id;
        this.size = size;
        this.quantityInStock = quantityInStock;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    @Override
    public String toString() {
        return "FormAddSize{" +
                "_id='" + _id + '\'' +
                ", size=" + size +
                ", quantityInStock=" + quantityInStock +
                '}';
    }
}
