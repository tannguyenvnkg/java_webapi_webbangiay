package com.teacup.WebServerBanGiay.models.datas;

import org.springframework.web.multipart.MultipartFile;

public class FormProduct {
    private MultipartFile file;
    private String productName;
    private int price;
    private String categoryId;
    private String providerId;

    public FormProduct() {

    }

    public FormProduct(MultipartFile file, String productName, int price, String categoryId, String providerId) {
        this.file = file;
        this.productName = productName;
        this.price = price;
        this.categoryId = categoryId;
        this.providerId = providerId;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    @Override
    public String toString() {
        return "FormProduct{" +
                "file=" + file +
                ", productName='" + productName + '\'' +
                ", price=" + price +
                ", categoryId='" + categoryId + '\'' +
                ", providerId='" + providerId + '\'' +
                '}';
    }
}
