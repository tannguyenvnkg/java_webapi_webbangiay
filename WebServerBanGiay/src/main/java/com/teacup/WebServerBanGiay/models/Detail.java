package com.teacup.WebServerBanGiay.models;

import org.springframework.data.annotation.Id;

public class Detail {
    // Đây là details gồm số size giày và số lượng khách hàng mua

    @Id
    private String sizeId;
    private int size;
    private int amount;

    public Detail() {

    }

    public Detail(String sizeId, int size, int amount) {
        this.sizeId = sizeId;
        this.size = size;
        this.amount = amount;
    }

    public String getSizeId() {
        return sizeId;
    }

    public void setSizeId(String sizeId) {
        this.sizeId = sizeId;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Detail{" +
                "sizeId='" + sizeId + '\'' +
                ", size=" + size +
                ", amount=" + amount +
                '}';
    }
}
