package com.teacup.WebServerBanGiay.models.formView;

import com.teacup.WebServerBanGiay.models.Details;

import java.util.List;

public class UpdateProduct {
    private String _id;
    private String productName;
    private int price;
    private String categoryId;
    private String providerId;
    private List<Details> details;
    private boolean status;

    public UpdateProduct() {

    }

    public UpdateProduct(String _id, String productName, int price, String categoryId, String providerId, List<Details> details, boolean status) {
        this._id = _id;
        this.productName = productName;
        this.price = price;
        this.categoryId = categoryId;
        this.providerId = providerId;
        this.details = details;
        this.status = status;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public List<Details> getDetails() {
        return details;
    }

    public void setDetails(List<Details> details) {
        this.details = details;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UpdateProduct{" +
                "_id='" + _id + '\'' +
                ", productName='" + productName + '\'' +
                ", price=" + price +
                ", categoryId='" + categoryId + '\'' +
                ", providerId='" + providerId + '\'' +
                ", details=" + details +
                ", status=" + status +
                '}';
    }
}
