package com.teacup.WebServerBanGiay.services;

import org.apache.tomcat.jni.File;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Stream;

@Service
public class ImageStoregeService implements IStorageService{

    private final Path storageFolder = Paths.get("image/product");
    //contructor
    public ImageStoregeService() {
        try {
            Files.createDirectories(storageFolder);
        } catch (IOException e) {
            throw new RuntimeException("Cannot initialize storage", e);
        }
    }

    @Override
    public String storeFile(MultipartFile file) {
        try {
            // check empty
            if (file.isEmpty()) {
                throw new RuntimeException("Vui Lòng chọn file Hình");
            }
            //check is this image ?
            if (!isImageFile(file)) {
                throw new RuntimeException("Bạn nên bọn file hình dạng png và jpg");
            }
            // Rename file
            String newNameFile = renameFile(file);

            Path destinationFilePath = this.storageFolder.resolve(
                    Paths.get(newNameFile))
                    .normalize().toAbsolutePath();

            if (!destinationFilePath.getParent().equals(this.storageFolder.toAbsolutePath())) {
                throw new RuntimeException("Không thể lưu file ngoài thư mục hiện tại");
            }
            try (InputStream inputStream = file.getInputStream())  {
                Files.copy(inputStream, destinationFilePath, StandardCopyOption.REPLACE_EXISTING);
            }
            return newNameFile;
        } catch (IOException ex) {
            throw new RuntimeException("không lưu được file", ex);
        }
    }

    @Override
    public byte[] readFileContent(String fileName) {
        try {
            Path file = storageFolder.resolve(fileName);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists() || resource.isReadable()) {
                byte[] bytes = StreamUtils.copyToByteArray(resource.getInputStream());
                return bytes;
            }
            else {
                throw new RuntimeException("Không thể đọc file: " + fileName);
            }
        } catch (IOException e) {
            throw new RuntimeException("Không thể đọc file: " + fileName, e);
        }
    }

    @Override
    public void deleteAllFile() {

    }

    //check is this image ?
    private boolean isImageFile(MultipartFile file) {

        String[] parts = file.getOriginalFilename().split("\\."); // cắt chuỗi lấy đuôi

        return Arrays.asList(new String[] {"png","jpg"}).contains(parts[1].trim().toLowerCase());
    }

    // Rename file = Chuỗi thời gian millisecond + tên file hình
    private String renameFile(MultipartFile file) {
        Date currentDate = new Date();
        return currentDate.getTime() + file.getOriginalFilename();
    }
}
