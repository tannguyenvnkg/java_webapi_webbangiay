package com.teacup.WebServerBanGiay.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class Mail {

    @Autowired
    private JavaMailSender emailSender;
    private static final String SUBJECT = "Tea Cup Shoes Store";
    private static final String SENDER = "teacupmusicmp3@gmail.com";

    public Mail() {
    }

    public boolean sendMail(String message,String receiveEmail){
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(SENDER);
        simpleMailMessage.setTo(receiveEmail);
        simpleMailMessage.setSubject(SUBJECT);
        simpleMailMessage.setText(message);
        this.emailSender.send(simpleMailMessage);
        return true;
    }
}
