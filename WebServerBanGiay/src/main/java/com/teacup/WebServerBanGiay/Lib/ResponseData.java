package com.teacup.WebServerBanGiay.Lib;

import com.teacup.WebServerBanGiay.models.ResponseObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseData {

    //return 3 values [error,message,data]
    public static ResponseEntity<ResponseObject> returnSuccessData(String message, Object data){ // error: false
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(false,message, data)
        );
    }

    //return 2 values [error,message]
    public static ResponseEntity<ResponseObject> returnErrorData(String message){ // error: true
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(true,message,"")
        );
    }
}
