package com.teacup.WebServerBanGiay;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebServerBanGiayApplication  {

	public static void main(String[] args) {
		SpringApplication.run(WebServerBanGiayApplication.class, args);
	}


}
